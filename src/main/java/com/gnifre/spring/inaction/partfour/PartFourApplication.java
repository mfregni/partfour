package com.gnifre.spring.inaction.partfour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * The tag @SpringBootApplication is a composite application that combines three other
 * annotations:
 * <p>
 * first @SpringBootConfiguration—Designates this class as a configuration class.
 * Although there’s not much configuration in the class yet, you can add Javabased
 * Spring Framework configuration to this class if you need to. This annotation
 * is, in fact, a specialized form of the @Configuration annotation.
 * <p>
 * then @EnableAutoConfiguration—Enables Spring Boot automatic configuration.
 * We’ll talk more about autoconfiguration later. For now, know that this annotation
 * tells Spring Boot to automatically configure any components that it thinks
 * you’ll need.
 * <p>
 * finally, @ComponentScan—Enables component scanning. This lets you declare other
 * classes with annotations like @Component, @Controller, @Service, and others,
 * to have Spring automatically discover them and register them as components in
 * the Spring application context.
 *
 * By extending WebMvcConfigurer we can delete the HomeController as used in chaper1
 * and replace it with addViewControllers method.
 */
@SpringBootApplication
public class PartFourApplication implements WebMvcConfigurer {
    // Initialize Spring Context, passing the configuration class and command line parameters
    public static void main(String[] args) {
        SpringApplication.run(PartFourApplication.class, args);
    }

    /**
     * Eliminate the need of HomeController as if for chapter1: because the HomeController simply do a forward,
     * that can be managed with this method.
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("/home");
        registry.addViewController("/login");
    }
}
