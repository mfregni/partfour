package com.gnifre.spring.inaction.partfour.configurations;

import com.gnifre.spring.inaction.partfour.security.UserRepositoryUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepositoryUserDetailsService userRepositoryUserDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userRepositoryUserDetailsService)
                .passwordEncoder(encoder());

    }

    /**
     * Using @Bean annotation allows us to use PasswordEncoder everywhere
     * @return
     */
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                //
                // Private
                .antMatchers("/users", "/organizations", "/roles", "/user/*", "/organizations/*", "/roles/*")
                .access("hasRole('ROLE_admin')")
                //
                // Public
                .antMatchers("/", "/home").access("permitAll")
                //
                // login
                .and()
                .formLogin()
                .loginPage("/login")
                //
                // logout
                .and()
                .logout()
                .logoutSuccessUrl("/")
                //
                // Make H2-Console non-secured; for debug purposes
                .and()
                .csrf()
                .ignoringAntMatchers("/h2-console/**")
                //
                // Allow pages to be loaded in frames from the same origin; needed for H2-Console
                .and()
                .headers()
                .frameOptions()
                .sameOrigin()
        ;
    }
}
