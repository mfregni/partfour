package com.gnifre.spring.inaction.partfour.controllers;


import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;

import com.gnifre.spring.inaction.partfour.model.Address;
import com.gnifre.spring.inaction.partfour.model.Organization;

import com.gnifre.spring.inaction.partfour.model.User;
import com.gnifre.spring.inaction.partfour.services.OrganizationService;

import com.gnifre.spring.inaction.partfour.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping
public class OrganizationsController {
    private final OrganizationService organizationService;
    private final UserService userService;


    private static final int PAGE_SIZE=3;

    public OrganizationsController(OrganizationService organizationService, UserService userService) {
        this.organizationService = organizationService;
        this.userService = userService;

    }

    @GetMapping("/organizations")
    public String showOrganizations(Model model) {
        return showPaginatedOrganizations(1, "name", "asc", model);
    }

    @GetMapping("/organizations/page/{pageNo}")
    public String showPaginatedOrganizations(@PathVariable(value = "pageNo") int pageNo,
                                     @RequestParam("sortField") String sortField,
                                     @RequestParam("sortDir") String sortDirection,
                                     Model model) {
        Page<Organization> page = organizationService.findPaginated(pageNo, PAGE_SIZE, sortField, sortDirection);
        model.addAttribute("organizationList", page.getContent());

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalElements", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDirection);
        model.addAttribute("reverseSortDir", sortDirection.equals("asc") ? "desc" : "asc");

        return "/organizations";
    }


    @GetMapping("/organization/new")
    public String showNewOrganizationForm(Model model) {
        model.addAttribute("organization", new Organization(new Address()));
        model.addAttribute("otherOrganizationList", organizationService.getAllOrganizations());
        return "/organization/new";
    }

    /***
     *
     * @param organization the information on the new organization (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     *
     * @return the organization page in order to show the new organization in the organizations list.
     */
    @PostMapping("/organization/new")
    public String processNewOrganization(@Valid Organization organization, Errors errors, Model model) {
        model.addAttribute("otherOrganizationList", organizationService.getAllOrganizations());
        if (errors.hasErrors()) {
            return "/organization/new";
        }
        try {
            if(organization.getParentOrganization() != null){
                if(organization.getParentOrganization().getId()!= null) {
                    organization.setParentOrganization(organizationService.findById(organization.getParentOrganization().getId()));
                }else {
                    // Default combobox Value
                    organization.setParentOrganization(null);
                }
            }
        } catch (OrganizationNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }
        log.info("Processing organization: " + organization);

        organizationService.saveOrUpdate(organization);

        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/organizations";
    }

    @GetMapping("/organization/edit/{id}")
    public String showEditOrganizationForm(@PathVariable(value = "id") Long id, Model model) {
        Organization organization = organizationService.findById(id);
        model.addAttribute("organization", organization);
        model.addAttribute("otherOrganizationList", organizationService.getOtherOrganizationList(id));

        return "/organization/edit";
    }

    /***
     *
     * @param organization the information on the organization (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     *
     * in thymeleaf we have to add: <input type="hidden" th:field="*{address.id}" />
     *               to transmit the address id to update the correct address, otherwise the id is null
     *
     * @return the organizations page in order to show the updated organization in the organizations list.
     */
    @PostMapping("/organization/edit/{id}")
    public String processUpdateOrganization(@Valid Organization organization, Errors errors, Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        model.addAttribute("otherOrganizationList", organizationService.getOtherOrganizationList(organization.getId()));

        if (errors.hasErrors()) {
            return "/organization/edit";
        }
        log.info("Updating organization: " + organization);
        try {
            if(organization.getParentOrganization() != null){
                if(organization.getParentOrganization().getId()!= null) {
                    organization.setParentOrganization(organizationService.findById(organization.getParentOrganization().getId()));
                }else {
                    // Default combobox Value
                    organization.setParentOrganization(null);
                }
            }
        } catch (OrganizationNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }

        organizationService.saveOrUpdate(organization);

        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/organizations";
    }

    @GetMapping("/organization/delete/{id}")
    public String processDeleteOrganization(@PathVariable("id") Long id, Model model) {
        organizationService.deleteById(id);
        return "redirect:/organizations";
    }

}
