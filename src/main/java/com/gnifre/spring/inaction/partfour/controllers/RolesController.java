package com.gnifre.spring.inaction.partfour.controllers;


import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.exceptions.RoleNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Address;
import com.gnifre.spring.inaction.partfour.model.Role;
import com.gnifre.spring.inaction.partfour.model.User;
import com.gnifre.spring.inaction.partfour.services.OrganizationService;
import com.gnifre.spring.inaction.partfour.services.RoleService;
import com.gnifre.spring.inaction.partfour.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Slf4j
@Controller
@RequestMapping
public class RolesController {
    private final RoleService roleService;
    private final UserService userService;


    private static final int PAGE_SIZE=3;

    public RolesController(RoleService roleService, UserService userService) {
        this.roleService = roleService;
        this.userService = userService;

    }

    @GetMapping("/roles")
    public String showRoles(Model model) {
        return showPaginatedRoles(1, "name", "asc", model);
    }

    @GetMapping("/roles/page/{pageNo}")
    public String showPaginatedRoles(@PathVariable(value = "pageNo") int pageNo,
                                     @RequestParam("sortField") String sortField,
                                     @RequestParam("sortDir") String sortDirection,
                                     Model model) {
        Page<Role> page = roleService.findPaginated(pageNo, PAGE_SIZE, sortField, sortDirection);
        model.addAttribute("roleList", page.getContent());

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalElements", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDirection);
        model.addAttribute("reverseSortDir", sortDirection.equals("asc") ? "desc" : "asc");

        return "/roles";
    }


    @GetMapping("/role/new")
    public String showNewRoleForm(Model model) {
        model.addAttribute("role", new Role());
        model.addAttribute("otherRoleList", roleService.getAllRoles());
        model.addAttribute("userList", userService.getAllUsers());
        return "/role/new";
    }

    /***
     *
     * @param role the information on the new role (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     *
     * @return the roles page in order to show the new role in the roles list.
     */
    @PostMapping("/role/new")
    public String processNewRole(@Valid Role role, Errors errors, Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        model.addAttribute("otherRoleList", roleService.getAllRoles());
        if (errors.hasErrors()) {
            return "/role/new";
        }
        try {
            if(role.getParentRole() != null){
                if(role.getParentRole().getId()!= null) {
                    role.setParentRole(roleService.findById(role.getParentRole().getId()));
                }else {
                    // Default combobox Value
                    role.setParentRole(null);
                }
            }
        } catch (RoleNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }
        Set<User> userSet = role.getUserSet();
        userSet.forEach(user -> user.getRoleSet().add(role));

        log.info("Processing role: " + role);

        roleService.saveOrUpdate(role);
        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/roles";
    }

    @GetMapping("/role/edit/{id}")
    public String showEditRoleForm(@PathVariable(value = "id") Long id, Model model) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        model.addAttribute("otherRoleList", roleService.getOtherRoleList(id));
        model.addAttribute("userList", userService.getAllUsers());

        return "/role/edit";
    }

    /***
     *
     * @param role the information on the role (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     *
     * in thymeleaf we have to add: <input type="hidden" th:field="*{address.id}" />
     *               to transmit the address id to update the correct address, otherwise the id is null
     *
     * @return the roles page in order to show the updated role in the roles list.
     */
    @PostMapping("/role/edit/{id}")
    public String processUpdateRole(@Valid Role role, Errors errors, Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        model.addAttribute("otherRoleList", roleService.getOtherRoleList(role.getId()));

        if (errors.hasErrors()) {
            return "/role/edit";
        }
        log.info("Updating role: " + role);
        try {
            if(role.getParentRole() != null){
                if(role.getParentRole().getId()!= null) {
                    role.setParentRole(roleService.findById(role.getParentRole().getId()));
                }else {
                    // Default combobox Value
                    role.setParentRole(null);
                }
            }
        } catch (RoleNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }

        for (User user: userService.getAllUsers()) {
            if(role.getUserSet().contains(user)) {
                user.getRoleSet().add(role);
            }else{
                    user.getRoleSet().remove(role);
            }
            userService.saveOrUpdate(user);

        }
        roleService.saveOrUpdate(role);
        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/roles";
    }

    @GetMapping("/role/delete/{id}")
    public String processDeleteRole(@PathVariable("id") Long id, Model model) {
        Role role = roleService.findById(id);
        // remove the relationship with all the related user
        for (User user:role.getUserSet()) {
            user.getRoleSet().remove(role);
            userService.saveOrUpdate(user);
        }
        roleService.deleteById(id);
        return "redirect:/roles";
    }
}
