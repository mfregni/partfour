package com.gnifre.spring.inaction.partfour.controllers;


import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Address;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.User;
import com.gnifre.spring.inaction.partfour.services.OrganizationService;
import com.gnifre.spring.inaction.partfour.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping
public class UserOrganizationController {
    private final OrganizationService organizationService;
    private final UserService userService;

    public UserOrganizationController(OrganizationService organizationService, UserService userService) {
        this.organizationService = organizationService;
        this.userService = userService;

    }

    @GetMapping("/organization/user_organization")
    public String showUserOrganization(Model model) {
        UserListDto userListDto = new UserListDto();
        userListDto.setUserList(userService.getAllUsers());
        model.addAttribute("userListDto", userListDto);
        model.addAttribute("organizationList", organizationService.getAllOrganizations());
        return "/organization/user_organization";
    }


    /***
     *
     * @param userListDto the information on update user's organization
     * @param errors if there are some bean validation rules broken
     *
     * @return the organization page in order to show the new organization in the organizations list.
     */
    @PostMapping("/organization/user_organization")
    public String processUserOrganization(UserListDto userListDto, Errors errors, Model model) {
        for (User currentUser: userListDto.getUserList()) {
            User user = userService.findById(currentUser.getId());
            Organization organization = organizationService.findById(currentUser.getOrganization().getId());
            user.setOrganization(organization);
            userService.saveOrUpdate(user);
        }
       

        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/organization/user_organization";
    }

}

class UserListDto{
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}