package com.gnifre.spring.inaction.partfour.controllers;


import com.gnifre.spring.inaction.partfour.configurations.propertyhandlers.UsersProperties;
import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Address;
import com.gnifre.spring.inaction.partfour.model.User;
import com.gnifre.spring.inaction.partfour.services.OrganizationService;
import com.gnifre.spring.inaction.partfour.services.RoleService;
import com.gnifre.spring.inaction.partfour.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping
public class UsersController {
    private final UserService userService;
    private final OrganizationService organizationService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
private final UsersProperties usersProperties;



    public UsersController(OrganizationService organizationService, UserService userService, RoleService roleService, PasswordEncoder passwordEncoder, UsersProperties usersProperties) {
        this.organizationService = organizationService;
        this.userService = userService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.usersProperties = usersProperties;
    }

    @GetMapping("/users")
    public String showUsers(Model model) {
        return showPaginatedUsers(1, "username", "asc", model);
    }

    @GetMapping("/users/page/{pageNo}")
    public String showPaginatedUsers(@PathVariable(value = "pageNo") int pageNo,
                                     @RequestParam("sortField") String sortField,
                                     @RequestParam("sortDir") String sortDirection,
                                     Model model) {
        Page<User> page = userService.findPaginated(pageNo, usersProperties.getPageSize(), sortField, sortDirection);
        model.addAttribute("userList", page.getContent());

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalElements", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDirection);
        model.addAttribute("reverseSortDir", sortDirection.equals("asc") ? "desc" : "asc");

        return "/users";
    }


    @GetMapping("/user/new")
    public String showNewUserForm(Model model) {
        model.addAttribute("user", new User(new Address()));
        model.addAttribute("organizationList", organizationService.getAllOrganizations());
        model.addAttribute("roleList", roleService.getAllRoles());
        return "/user/new";
    }

    /***
     *
     * @param user the information on the new user (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     * @param authenticatedUser authenticated user who performs the operation
     * @return the users page in order to show the new user in the users list.
     */
    @PostMapping("/user/new")
    public String processNewUser(@Valid User user, Errors errors, Model model, @AuthenticationPrincipal User authenticatedUser) {
        model.addAttribute("organizationList", organizationService.getAllOrganizations());
        model.addAttribute("roleList", roleService.getAllRoles());

        if (errors.hasErrors()) {
            return "/user/new";
        }
        log.info("User: " + authenticatedUser + " is creating a new  user: " + user);
        try {
            user.setOrganization(organizationService.findById(user.getOrganization().getId()));
        } catch (OrganizationNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.saveOrUpdate(user);
        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/users";
    }

    @GetMapping("/user/edit/{id}")
    public String showEditUserForm(@PathVariable(value = "id") Long id, Model model) {
        User user = userService.findById(id);
        model.addAttribute("user", user);
        model.addAttribute("organizationList", organizationService.getAllOrganizations());
        model.addAttribute("roleList", roleService.getAllRoles());
        return "/user/edit";
    }

    /***
     *
     * @param user the information on the user (@valid notation means to perform a validation check on bean validation rules)
     * @param errors if there are some bean validation rules broken
     *
     * in thymeleaf we have to add: <input type="hidden" th:field="*{address.id}" />
     *               to transmit the address id to update the correct address, otherwise the id is null
     *
     * @return the users page in order to show the updated user in the users list.
     */
    @PostMapping("/user/edit/{id}")
    public String processUpdateUser(@Valid User user, Errors errors, Model model) {
        model.addAttribute("organizationList", organizationService.getAllOrganizations());
        model.addAttribute("roleList", roleService.getAllRoles());
        if (errors.hasErrors()) {
            return "/user/edit";
        }
        log.info("Updating user: " + user);
        // Change Organization
        try {
            user.setOrganization(organizationService.findById(user.getOrganization().getId()));
        } catch (OrganizationNotFoundException e) {
            log.error(e.getLocalizedMessage(),e);
        }
        // Change roles
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.saveOrUpdate(user);
        // redirect: force the browser (the client) to do a new HTTP Request
        // forward: goto the page without asking the client to do a new HTTP Request (the movement is internally on the server)
        // see \resources\static\images\Redirect vs forward.png image
        return "redirect:/users";
    }

    @GetMapping("/user/delete/{id}")
    public String processDeleteUser(@PathVariable("id") Long id, Model model) {
        userService.deleteById(id);
        return "redirect:/users";
    }
}
