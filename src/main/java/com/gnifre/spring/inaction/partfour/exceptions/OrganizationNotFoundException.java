package com.gnifre.spring.inaction.partfour.exceptions;

public class OrganizationNotFoundException extends RuntimeException {

    public OrganizationNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public OrganizationNotFoundException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
