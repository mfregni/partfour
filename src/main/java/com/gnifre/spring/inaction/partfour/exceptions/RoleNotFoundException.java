package com.gnifre.spring.inaction.partfour.exceptions;

public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public RoleNotFoundException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
