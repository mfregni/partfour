package com.gnifre.spring.inaction.partfour.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * The @RequiredArgsConstructor create Constructor for final and @NotNull attributes
 *
 * The JPA requires that entities have a no-arguments constructor, so Lombok’s @NoArgsConstructor does the job.
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(of = { "id" })
@Entity
@Table(name = "ADDRESS")
public class Address {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Street address is required")
    private String street;

    @NotBlank(message = "Zip code is required")
    private String zip;

    @NotBlank(message = "City is required")
    private String city;

    private String province;

    @NotBlank(message = "Country is required")
    private String country;

    private String description;

    /**
     * Bidirectional relationship with user
     * The specification mappedBy = "address" means that the address side of the relationship is the non-owning side
     */
    @ToString.Exclude
    @OneToOne(mappedBy = "address")
    private User user;

    /**
     * Bidirectional relationship with organization
     * The specification mappedBy = "address" means that the address side of the relationship is the non-owning side
     */
    @ToString.Exclude
    @OneToOne(mappedBy = "address")
    private Organization organization;
}
