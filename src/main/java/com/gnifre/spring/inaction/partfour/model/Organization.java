package com.gnifre.spring.inaction.partfour.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * The @RequiredArgsConstructor create Constructor for final and @NotNull attributes
 *
 * The JPA requires that entities have a no-arguments constructor, so Lombok’s @NoArgsConstructor does the job.
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(of = { "id" })
@Entity
@Table(name = "ORGANIZATION")
public class Organization {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 5, message = "Organization name must be at least 5 characters long")
    private String name;

    @NotBlank(message = "Email code is required")
    @Email(message = "Enter a valid mail address")
    private String email;

    private String mobile;

    @NotBlank(message = "Organization telephone is required")
    private String telephone;

    private String fax;

    private String description;

    /**
     * Add @Valid to force Address bean validation
     * The annotation @JoinColumn is needed only on the owning side of the foreign key relationship
     * "OWNING" side is user because if you delete an organization you also delete the address. But if you delete the address you don't want to delete the organization
     */
    @NotNull(message = "Address is required")
    @Valid
    @ToString.Exclude
    @OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", nullable=false)
    private Address address;


    // cascade=CascadeType.DETACH to avoid delete of the parent when a child is removed.
    @ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name = "PARENT_ORGANIZATION_ID")
    private Organization parentOrganization;

    @ToString.Exclude
    @ManyToMany(mappedBy="parentOrganization")
    private Set<Organization> subOrganizationSet;

    @ToString.Exclude
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<User> userSet;

    public Organization(Address address) {
        this.address = address;
    }

}
