package com.gnifre.spring.inaction.partfour.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * The @RequiredArgsConstructor create Constructor for final and @NotNull attributes
 *
 * The JPA requires that entities have a no-arguments constructor, so Lombok’s @NoArgsConstructor does the job.
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(of = { "id" })
@Entity
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 5, message = "Role name must be at least 5 characters long")
    private String name;

    private String description;

    @NotNull(message = "RoleType is required")
    private RoleType type;

    // cascade=CascadeType.DETACH to avoid delete of the parent when a child is removed.
    @ManyToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="PARENT_ROLE_ID")
    private Role parentRole;

    @ToString.Exclude
    @OneToMany(mappedBy="parentRole")
    private Set<Role> subRoleSet;

    @ToString.Exclude
    @ManyToMany(mappedBy = "roleSet", cascade = { CascadeType.DETACH })
    Set<User> userSet;

    @Override
    public String getAuthority() {
        return name;
    }
}
