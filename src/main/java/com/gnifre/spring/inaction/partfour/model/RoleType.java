package com.gnifre.spring.inaction.partfour.model;

public enum RoleType {
    GENERIC,
    SYSTEM,
    USABILITY,
    APPLICABLE,
    APPLIED,
}
