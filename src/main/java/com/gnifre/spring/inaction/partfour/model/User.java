package com.gnifre.spring.inaction.partfour.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The @RequiredArgsConstructor create Constructor for final and @NotNull attributes
 *
 * The JPA requires that entities have a no-arguments constructor, so Lombok’s @NoArgsConstructor does the job.
 */
@Data
@RequiredArgsConstructor
@EqualsAndHashCode(of = { "id" })
@Entity
@Table(name = "USER")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 5, message = "Username must be at least 5 characters long")
    private String username;

    @NotNull
    @Size(min = 8, message = "Password must be at least 8 characters long")
    private String password;

    @NotBlank(message = "Name is required")
    private String name;

    @NotBlank(message = "Surname is required")
    private String surname;

    @NotBlank(message = "Email is required")
    @Email(message = "Enter a valid mail address")
    private String email;


    private String mobile;
    private String telephone;
    private String fax;
    private String description;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;

    /**
     * Add @Valid to force Address bean validation
     * The annotation @JoinColumn is needed only on the "owning" side of the foreign key relationship
     * The tag @ToString.Exclude avoid stack overflow error due to recursive call of toString(toString(...
     * cascade=CascadeType.ALL  in order to create ad delete the address row when a new user row is created or deleted
     */
    @NotNull(message = "Address is required")
    @Valid
    @ToString.Exclude
    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", nullable=false)
    private Address address;

    /**
     * it's a good practice to mark many-to-one side as the "owning" side.
     * The annotation @JoinColumn is needed only on the "owning" side of the foreign key relationship
     */
    @NotNull(message = "Organization is required")
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name="ORGANIZATION_ID", nullable=false)
    private Organization organization;

    /**
     * The annotation @JoinColumn is needed only on the "owning" side of the foreign key relationship
     * The @JoinTable is used to define the join/link table
     *
     * fetch = FetchType.EAGER to load roles for user authentication
     */
    @ToString.Exclude
    @ManyToMany(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
    @JoinTable(
            name = "USER_ROLE",
            joinColumns = { @JoinColumn(name = "USER_ID") },
            inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") }
    )
    private Set<Role> roleSet = new HashSet<>();

    public User(Address address) {
        this.address = address;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roleSet;
    }
}
