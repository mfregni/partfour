package com.gnifre.spring.inaction.partfour.repositories;

import com.gnifre.spring.inaction.partfour.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JpaRepository extends PagingAndSortingRepository that extends CrudRepository. It provides all the methods for implementing the pagination.
 */
public interface AddressRepository extends JpaRepository<Address, Long> {
}
