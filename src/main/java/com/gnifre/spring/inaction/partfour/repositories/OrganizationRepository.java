package com.gnifre.spring.inaction.partfour.repositories;

import com.gnifre.spring.inaction.partfour.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * JpaRepository extends PagingAndSortingRepository that extends CrudRepository. It provides all the methods for implementing the pagination.
 */
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    @Query("SELECT o FROM Organization o WHERE o.id <> ?1")
    List<Organization> getOtherOrganizationList(Long id);
}
