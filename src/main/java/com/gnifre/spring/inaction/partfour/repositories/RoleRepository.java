package com.gnifre.spring.inaction.partfour.repositories;

import com.gnifre.spring.inaction.partfour.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * JpaRepository extends PagingAndSortingRepository that extends CrudRepository. It provides all the methods for implementing the pagination.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("SELECT r FROM Role r WHERE r.id <> ?1")
    List<Role> getOtherRoleList(Long id);

}
