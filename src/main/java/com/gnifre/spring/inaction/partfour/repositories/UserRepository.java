package com.gnifre.spring.inaction.partfour.repositories;

import com.gnifre.spring.inaction.partfour.model.Address;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * JpaRepository extends PagingAndSortingRepository that extends CrudRepository.
 * It provides all the methods for implementing the pagination.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByOrganization(Organization organization);

    User findByName(String name);
}
