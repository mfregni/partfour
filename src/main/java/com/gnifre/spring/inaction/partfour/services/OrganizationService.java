package com.gnifre.spring.inaction.partfour.services;

import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.exceptions.RoleNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrganizationService {
    Organization saveOrUpdate(Organization organization);

    List<Organization> getAllOrganizations();

    List<Organization> getOtherOrganizationList(Long id);

    void delete(Organization organization);

    void deleteById(Long id) throws OrganizationNotFoundException;

    Organization findById(Long id) throws OrganizationNotFoundException;

    Page<Organization> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
