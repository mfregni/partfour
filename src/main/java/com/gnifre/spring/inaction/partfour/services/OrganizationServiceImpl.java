package com.gnifre.spring.inaction.partfour.services;

import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.exceptions.UserNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.Role;
import com.gnifre.spring.inaction.partfour.repositories.OrganizationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationServiceImpl implements OrganizationService{

    private final OrganizationRepository organizationRepository;

    public OrganizationServiceImpl(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Override
    public Organization saveOrUpdate(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public List<Organization> getAllOrganizations() {
        return organizationRepository.findAll();
    }

    @Override
    public List<Organization> getOtherOrganizationList(Long id){
        return organizationRepository.getOtherOrganizationList(id);
    }

    @Override
    public void delete(Organization organization) {
        organizationRepository.delete(organization);
    }

    @Override
    public void deleteById(Long id) throws UserNotFoundException {
        organizationRepository.delete(findById(id));
    }

    @Override
    public Organization findById(Long id) throws OrganizationNotFoundException {
        return organizationRepository.findById(id).orElseThrow(() -> new OrganizationNotFoundException("Not found any Organization identified with an id: " + id));
    }

    @Override
    public Page<Organization> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {

        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);
        return organizationRepository.findAll(pageable);
    }
}
