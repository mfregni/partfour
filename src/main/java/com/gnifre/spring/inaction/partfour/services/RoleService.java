package com.gnifre.spring.inaction.partfour.services;

import com.gnifre.spring.inaction.partfour.exceptions.RoleNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Role;
import com.gnifre.spring.inaction.partfour.model.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
    Role saveOrUpdate(Role role);

    List<Role> getAllRoles();

    List<Role> getOtherRoleList(Long id);

    void delete(Role user);

    void deleteById(Long id) throws RoleNotFoundException;

    Role findById(Long id) throws RoleNotFoundException;

    Page<Role> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
