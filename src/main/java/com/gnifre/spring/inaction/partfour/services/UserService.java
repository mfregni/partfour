package com.gnifre.spring.inaction.partfour.services;

import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.exceptions.UserNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface UserService {
    User saveOrUpdate(User user);

    List<User> getAllUsers();

    void delete(User user);

    void deleteById(Long id) throws UserNotFoundException;

    User findById(Long id) throws UserNotFoundException;

    Page<User> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

    void assignUsersToOrganization(Organization organization, Set<User> userSet);
}
