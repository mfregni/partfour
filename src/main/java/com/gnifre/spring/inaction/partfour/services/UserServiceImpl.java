package com.gnifre.spring.inaction.partfour.services;

import com.gnifre.spring.inaction.partfour.exceptions.OrganizationNotFoundException;
import com.gnifre.spring.inaction.partfour.exceptions.UserNotFoundException;
import com.gnifre.spring.inaction.partfour.model.Organization;
import com.gnifre.spring.inaction.partfour.model.User;
import com.gnifre.spring.inaction.partfour.repositories.OrganizationRepository;
import com.gnifre.spring.inaction.partfour.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveOrUpdate(User user) {
        return userRepository.save(user);
    }


    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteById(Long id) throws UserNotFoundException {
        userRepository.delete(findById(id));
    }

    @Override
    public User findById(Long id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Not found any User identified with an id: " + id));
    }

    @Override
    public Page<User> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {

        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);
        return userRepository.findAll(pageable);
    }

    @Override
    public void assignUsersToOrganization(Organization organization, Set<User> userSet){
        List<User> userList = userRepository.findByOrganization(organization);
        for (User user: userList) {
            if(!userSet.contains(user)){
                // the user doesn't belongs anymore to the organization
                user.setOrganization(null);
                userRepository.save(user);
            }
        }
        // new relationship
        for (User user: userSet) {
            user.setOrganization(organization);
            userRepository.save(user);
        }
    }
}
